<?php
namespace App\Model;
use Nette;
/**
 * Description of ApplicationModel
 *
 * @author Apex
 */
class ApplicationModel {
    /** @var Nette\Database\Context @inject*/
    public $database;
    
    public function __construct(\Nette\Database\Context $db) {
        $this->database = $db;
    }

    /**
     * Build recursively tree of packages
     * @param array $elements
     * @param int $parent
     * @return array
     */
    private function buildTree(array $elements, $parent = 0) {
        $branch = array();
        
        foreach ($elements as $element) {
            if ($element['parent'] == $parent) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Search recursively tree
     * @param array $data
     * @param $packageId
     * @return mixed|null
     */
    private function searchPackage(array $data, $packageId){
        foreach ($data as $package) {
            if($package['id']==$packageId)
            { 
                return $package; 
            } else {
                if(isset($package['children'])){
                    $r = $this->searchPackage($package['children'], $packageId);
                    if ($r !== null){
                        return $r;
                    }
                } 
            }
        }
        return null;
    }

    /**
     * Search for users with accessible channel
     * @param $channel
     * @return array
     */
    public function getUsersByAccessibleChannel($channel){
        $data = $this->database->query(   "SELECT users.login, serv.user, serv.channelPackage, serv.searched_package FROM users JOIN "
                                        . "(SELECT services.user, services.channelPackage, pom.package as searched_package FROM `services` JOIN "
                                        . "(SELECT channelpackagechannels.package FROM channelpackagechannels WHERE channelpackagechannels.channel = ? ) AS pom WHERE services.from <= curdate() AND (services.to IS NULL OR services.to >= curdate())) AS serv "
                                        . "ON (users.id = serv.user)",$channel)->fetchAll();
        $packages = $this->database->table('channelpackages')->fetchAll();
        $packages = $this->toArray($packages);
        $users = array();
        foreach ($data as $row){
            if($row['channelPackage'] == $row['searched_package']){
                $users[] = $row['login'];
            } else {
                $tree = $this->buildTree($packages,$row['channelPackage']);
                if($this->searchPackage($tree, $row['searched_package'])!=null) $users[] = $row['login'];
            }
        }
        return $users;
    }

    /**
     * Search users which don't have active channel
     * @param $shortname
     * @return Nette\Database\IRow[]
     */
    public function getUsersWithoutActiveChannel($shortname){
        $data = $this->database->query("SELECT * FROM users WHERE users.id IN 
                                        (SELECT user AS id_user FROM `services` JOIN 
                                        (SELECT channelpackages.id AS package_id FROM channelpackages WHERE channelpackages.shortname = ?) AS search 
                                        WHERE ((services.to < CURDATE() AND services.to IS NOT NULL) AND services.channelPackage <> search.package_id) 
                                        OR (services.channelPackage = search.package_id AND (services.to > CURDATE() OR services.to IS NULL)))
                                        ", $shortname)->fetchAll();
        return $data;
    }

    /**
     * Counts days for each user with selected package
     * @param $shortname
     * @return array
     */
    public function getDayCountWithPackage($shortname){
        $data = $this->database->query("  SELECT services.user, users.login, services.channelPackage, services.from, services.to, searched_package FROM services 
                                          JOIN users ON (services.user = users.id) 
                                          JOIN (SELECT channelpackages.id AS searched_package FROM channelpackages WHERE channelpackages.shortname = ?) 
                                          AS Pom",$shortname)->fetchAll();
        $users = array();
        $packages = $this->database->table('channelpackages')->fetchAll();
        $packages = $this->toArray($packages);
        foreach ($data as $row){
            if($row["channelPackage"] == $row["searched_package"]){
               $users[] = $row;
            } else {
                $tree = $this->buildTree($packages,$row['channelPackage']);
                if($this->searchPackage($tree, $row['searched_package'])!=null) $users[] = $row;
            }
        }
        foreach($users as $user){
            if($user['to']==null) $user['to'] = new Nette\Utils\DateTime(date("Y-m-d"));
            $user['from'] = new Nette\Utils\DateTime($user['from']);
            $user['day_count'] = date_diff($user['to'],$user['from'])->days;
        }
        return $users;
    }

    /**
     * Method used for conversion table selection object to simple array
     * @param $source
     * @return mixed
     */
    private static function toArray($source) {

    if (is_object($source)) {
      list($source) = self::toArray(array($source));
    }

    if (!is_array($source)) {
      return $source;
    }

    foreach ($source as &$row) {
      if (is_object($row)) {
        $row = iterator_to_array($row);
      } else {
        $row = self::toArray($row);
      }
    }
    return $source;
  }
}
