<?php

namespace App\Presenters;

use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{
    /** @var  Model\ApplicationModel @inject */
    public $applicationModel;
    
    public function renderDefault()
	{
		$this->template->a = $this->applicationModel->getUsersByAccessibleChannel("ct4sport");
        $this->template->b = $this->applicationModel->getUsersWithoutActiveChannel('hbogo');
        $this->template->c = $this->applicationModel->getDayCountWithPackage('hbogo');
    }

}
